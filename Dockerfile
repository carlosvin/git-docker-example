FROM alpine

RUN apk update && apk upgrade && apk add --no-cache bash git openssh

ARG GIT_PASS
ARG GIT_USER
ARG GIT_HOST

ENV GIT_PASS $GIT_PASS
ENV HOME /root

RUN echo "https://${GIT_USER}:${GIT_PASS}@${GIT_HOST}" > $HOME/.git-credentials
RUN git config --global user.email "${GIT_USER}@gmail.com"
RUN git config --global user.name "${GIT_USER}"
RUN git config --global credential.helper "store"
RUN git clone https://${GIT_HOST}/cms2.git
WORKDIR cms2


RUN git pull
RUN date > test.txt
RUN git add .
RUN git commit . -m "dsf"
RUN git push
